import os
import logging
import asyncio
import sqlite3
import aiosqlite
from concurrent.futures import ThreadPoolExecutor
from kivymd.app import MDApp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.textfield import MDTextField
from kivymd.uix.button import MDIconButton
from kivymd.uix.list import MDList, OneLineListItem
from kivy.lang import Builder
from kivy.uix.screenmanager import Screen, ScreenManager
from kivy.uix.scrollview import ScrollView
from kivy.metrics import dp
from kivy.clock import mainthread
from llama_cpp import Llama
from datetime import datetime

logging.basicConfig(level=logging.INFO)

KV = '''
<ChatBubble@MDBoxLayout>:
    orientation: 'vertical'
    size_hint_y: None
    height: self.minimum_height
    padding: "10dp"
    spacing: "5dp"
    md_bg_color: app.theme_cls.bg_darkest
    canvas.before:
        Color:
            rgba: app.theme_cls.primary_color
        Line:
            width: 2
            rectangle: (self.x, self.y, self.width, self.height)

    MDLabel:
        id: message_label
        text: ""
        theme_text_color: "Custom"
        text_color: 1, 1, 1, 1
        size_hint_y: None
        height: self.texture_size[1]

    Widget:
        size_hint_y: None
        height: "5dp"

ScreenManager:
    id: screen_manager
    MainScreen:
        name: "main_screen"

<MainScreen>:
    BoxLayout:
        orientation: 'vertical'
        padding: dp(20)
        spacing: dp(10)
        canvas.before:
            Color:
                rgba: app.theme_cls.bg_dark
            Rectangle:
                size: self.size
                pos: self.pos

        MDLabel:
            id: status_label
            text: "Model loaded successfully."
            halign: 'center'
            font_style: 'H4'
            theme_text_color: "Primary"
            size_hint_y: None
            height: self.texture_size[1] + dp(10)

        ScrollView:
            id: chat_scroll
            MDList:
                id: chat_list
                spacing: dp(10)

        BoxLayout:
            orientation: 'horizontal'
            padding: dp(10)
            spacing: dp(10)

            MDTextField:
                id: message_input
                hint_text: "Type a message"
                icon_left: "message"
                icon_left_color: app.theme_cls.primary_color
                normal_color: app.theme_cls.bg_normal
                color_mode: 'primary'
                size_hint_x: 0.8

            MDIconButton:
                icon: "send"
                theme_text_color: "Custom"
                text_color: app.theme_cls.primary_color
                on_release: app.send_message()
'''

class ChatBubble(MDBoxLayout):
    pass

class MainScreen(Screen):
    pass

class ChatApp(MDApp):
    llama_model = None
    memory = []
    executor = ThreadPoolExecutor(max_workers=5)
    db_con = None

    def on_start(self):
        self.init_db()
        self.load_model()
        self.load_chat_history()

    def init_db(self):
        db_path = "chat_history.db"
        if not os.path.exists(db_path):
            self.db_con = sqlite3.connect(db_path)
            self.db_con.execute(
                """
                CREATE TABLE IF NOT EXISTS chat_history (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    message TEXT NOT NULL,
                    is_bot INTEGER NOT NULL,
                    timestamp TIMESTAMP DEFAULT CURRENT_TIMESTAMP
                )
                """
            )
            self.db_con.commit()
        else:
            self.db_con = sqlite3.connect(db_path)

    def load_model(self):
        self.executor.submit(self.load_llama_model)

    def load_llama_model(self):
        model_name = "llama-2-7b-chat.ggmlv3.q8_0.bin"
        if os.path.exists(model_name):
            self.llama_model = Llama(
                model_path=model_name
            )
        else:
            self.root.get_screen('main_screen').ids.status_label.text = "Error: Model file not found."

    def send_message(self):
        message_input = self.root.get_screen('main_screen').ids.message_input
        message = message_input.text.strip()
        if message:
            asyncio.create_task(self.save_and_display_message(message, is_bot=False))
            asyncio.create_task(self.generate_bot_response(message))
            message_input.text = ""

    async def save_and_display_message(self, message, is_bot=False):
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        async with aiosqlite.connect("chat_history.db") as db_con:
            await db_con.execute(
                "INSERT INTO chat_history (message, is_bot, timestamp) VALUES (?, ?, ?)",
                (message, 1 if is_bot else 0, timestamp)
            )
            await db_con.commit()
        await self.async_display_chat_message(f"You: {message}", is_bot)
        
        
        
    async def load_chat_history(self):
        async with aiosqlite.connect("chat_history.db") as db_con:
            async with db_con.execute("SELECT message, is_bot FROM chat_history") as cursor:
                async for row in cursor:
                    message, is_bot = row
                    await self.async_display_chat_message(message, is_bot)

    async def generate_bot_response(self, message):
        try:
            if self.llama_model:
                # Assuming llama_model returns a dictionary
                bot_response = self.llama_model(message)
            
                if isinstance(bot_response, dict):
                    bot_message = bot_response.get('text')
                    if bot_message:
                        await self.save_and_display_message(bot_message, is_bot=True)
                        logging.info(f"Bot Response: {bot_message}")
                    else:
                        error_message = 'Error: Missing text in response structure'
                        await self.display_chat_message(error_message, is_bot=True)
                        logging.error(error_message)
                else:
                    error_message = 'Error: Invalid response structure'
                    await self.display_chat_message(error_message, is_bot=True)
                    logging.error(error_message)
            else:
                error_message = "Error: Model not loaded."
                await self.display_chat_message(error_message, is_bot=True)
                logging.error(error_message)
        except Exception as e:
            error_message = f'Error: {str(e)}'
            await self.display_chat_message(error_message, is_bot=True)
            logging.exception("Exception in generate_bot_response")


    @mainthread
    def display_chat_message(self, message, is_bot=False):
        chat_list = self.root.get_screen('main_screen').ids.chat_list
        chat_bubble = ChatBubble()
        if chat_bubble:
            chat_bubble.ids.message_label.text = message
            chat_list.add_widget(chat_bubble)
            chat_scroll = self.root.get_screen('main_screen').ids.chat_scroll
            chat_scroll.scroll_y = 0
        else:
            logging.warning("Failed to create ChatBubble instance.")
            
    async def async_display_chat_message(self, message, is_bot=False):
        self.display_chat_message(message, is_bot)

    async def run_async_tasks(self):
        await self.load_chat_history()

    def run_tasks(self):
        asyncio.run(self.run_async_tasks())

    def build(self):
        self.theme_cls.theme_style = "Dark"
        self.theme_cls.primary_palette = "Indigo"
        self.run_tasks()
        return Builder.load_string(KV)

if __name__ == "__main__":
    ChatApp().run()